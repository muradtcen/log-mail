<?php

namespace LogicSource\LogMail\Mail;


use Monolog\Handler\AbstractHandler;

class LogMailHandler extends AbstractHandler
{
    public function handle(array $record)
    {
        $exception = array_get($record, 'context.exception');


        $message = \Request::userAgent() !== 'Symfony/3.X'
            ? array_get($record, 'message') . 'Request path: ' . \Request::path()
            : array_get($record, 'message') . " через консоль";


        $trace = $exception ? $exception->getTraceAsString() : '';

        $emailArray = [];
        foreach (explode(',', env('MAIL_ADMIN')) as $item){
            $emailArray []= ['email' => $item, 'name' => ''];
        }

        \Mail::bcc($emailArray)->queue(new LogMail(
            $message,
            array_get($record, 'datetime')->format('Y-m-d H:i:s'),
            $trace
        ));
    }

}